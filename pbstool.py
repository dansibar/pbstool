#!/usr/bin/env python
"""
Utility script for pbs

Usage:
  pbstool qstat [<user> [<jobname>]]
  pbstool qdel  <jobname> [--dryrun]
  pbstool qhold <jobname> [--dryrun]
  pbstool qrls  <jobname> [--dryrun]
  pbstool hq [--dryrun]
  pbstool rh [--dryrun]
  pbstool dh [--dryrun]

Options:
  -h --help      Show this screen.
  --version      Show version.
  <user>         user name
  <jobname>      name of submitted job
  --dryrun       print commands but dont execute
  hq             hold all queued jobs for user
  rh             release all held jobs for user
  dh             delete all held jobs for user

"""
from docopt import docopt
from pbs_logging import setup_logger
import subprocess
from tabulate import tabulate
from collections import defaultdict
from os import system
from getpass import getuser

logger = setup_logger(__name__)


def sanitize(string):
    """
    Convert docopt args to valid python variable names
    """
    return string.strip('<>-').replace('-', '_').lower()


def flatten(d, out=[]):
    """
    return list of whatever is at the bottom
    """
    if isinstance(d, dict):
        for val in d.values():
            flatten(val, out)
    else:
        out += d
    return out


class Pbs():

    def __init__(self):
        self.qstat_raw_out = subprocess.Popen(
            ["qstat"], stdout=subprocess.PIPE).communicate()[0]
        self.qstat_out = None
        self.username = getuser()
        self.headers = ['Job id', 'Name', 'User', 'Time Use', 'S', 'Queue']
        self.process_qstat()
        self.format_qstat()

    def process_qstat(self):
        self.qstat_out = [
            item.split() for item in self.qstat_raw_out.split('\n')]
        # remove headers
        self.qstat_out.pop(0)
        self.qstat_out.pop(0)
        self.qstat_out.pop()

    def format_qstat(self):
        self.qstat_dict = defaultdict(lambda: defaultdict(list))
        for i in self.qstat_out[1:]:
            self.qstat_dict[i[2]][i[1]].append(i)

    def qstat(self, user, jobname, **kwargs):
        to_print = self.qstat_dict
        if user:
            to_print = to_print[user]
        if jobname:
            to_print = to_print[jobname]
        print tabulate(flatten(to_print), headers=self.headers)

    def exe_cmd(self, jobname, dryrun, cmd, jobs=None, **kwargs):
        if jobs is None:
            jobs = self.qstat_dict[self.username][jobname]
        print tabulate(jobs, headers=self.headers)
        if dryrun:
            return
        print 'run {} - continue y/n'.format(cmd)
        if raw_input() == 'y':
            for i in jobs:
                print '{} {}'.format(cmd, i[0])
                system('{} {}'.format(cmd, i[0]))

    def qdel(self, jobname, dryrun, **kwargs):
        self.exe_cmd(jobname, dryrun, 'qdel', **kwargs)

    def qrls(self, jobname, dryrun, **kwargs):
        self.exe_cmd(jobname, dryrun, 'qrls', **kwargs)

    def qhold(self, jobname, dryrun, **kwargs):
        self.exe_cmd(jobname, dryrun, 'qhold', **kwargs)

    def hold_queued(self, dryrun, **kwargs):
        jobs = self.qstat_dict[self.username]
        jobs = [job for job in flatten(jobs) if job[4] == 'Q']
        self.exe_cmd(dryrun=dryrun, cmd='qhold', jobs=jobs, **kwargs)

    def release_held(self, dryrun, **kwargs):
        jobs = self.qstat_dict[self.username]
        jobs = [job for job in flatten(jobs) if job[4] == 'H']
        self.exe_cmd(dryrun=dryrun, cmd='qrls', jobs=jobs, **kwargs)

    def del_held(self, dryrun, **kwargs):
        jobs = self.qstat_dict[self.username]
        jobs = [job for job in flatten(jobs) if job[4] == 'H']
        self.exe_cmd(dryrun=dryrun, cmd='qdel', jobs=jobs, **kwargs)

if __name__ == '__main__':
    ARGS = docopt(__doc__)
    for arg in ARGS:
        ARGS[sanitize(arg)] = ARGS.pop(arg)
    logger.info('Started')
    pbs = Pbs()
    logger.debug(ARGS)
    if ARGS['qstat']:
        pbs.qstat(**ARGS)
    if ARGS['qdel']:
        pbs.qdel(**ARGS)
    if ARGS['qhold']:
        pbs.qhold(**ARGS)
    if ARGS['qrls']:
        pbs.qrls(**ARGS)
    if ARGS['hq']:
        pbs.hold_queued(**ARGS)
    if ARGS['rh']:
        pbs.release_held(**ARGS)
    if ARGS['dh']:
        pbs.del_held(**ARGS)
